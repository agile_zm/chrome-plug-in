// 图组
const imgList = [
	'https://img.mp.itc.cn/upload/20170325/f38d14715d7f4ab7b41e6a43f07373bc_th.jpg',
	'https://ss2.baidu.com/-vo3dSag_xI4khGko9WTAnF6hhy/zhidao/pic/item/fc1f4134970a304e809c8570d0c8a786c8175cf9.jpg',
	'https://n.sinaimg.cn/sinacn14/694/w876h618/20180530/0994-hcffhsv6275677.png'
]

// 获取HTML文档中的第一个<body>元素
let body = document.getElementsByTagName('body')[0];

// 创建一个 div 元素，设置属性
let myBoxImg = document.createElement('div');
myBoxImg.id = 'myBoxImg';
myBoxImg.style.position = 'fixed';
myBoxImg.style.right = '40px';
myBoxImg.style.top = '50%';
myBoxImg.style.opacity = '0.9';
myBoxImg.style.zIndex = '1000';

// 追加到body中
body.appendChild(myBoxImg);

/**
 * 轮播图
 * @param i 当前索引
 * @param min 最小索引
 * @param max 最大索引
 * @param file 文件名字
 */
function changeImg(i,min,max,file){

	myBoxImg.innerHTML = `<img id="mypet" style="width:250px !important;height:134px !important;" src="${imgList[i]}" alt=""/>`

	i = (i < max) ? i+1 : min

	setTimeout(() => changeImg(i, min, max, file), 200)

}
changeImg(0,0,imgList.length-1,"水门");

let isMove = false;
let startX, startY, endX, endY, _gx, _gy;

//按钮拖拽功能

// 缓存选择器
const $myBoxImg = $("#myBoxImg");
$myBoxImg.mousedown(function(e){

	isMove=true;
	startX = e.pageX
	startY = e.pageY
	console.log("move",isMove);
	_gx=e.pageX-parseInt($myBoxImg.css("left"));
	_gy=e.pageY-parseInt($myBoxImg.css("top"));
});
$(document).mousemove(function(e){
	if(isMove){
		const x = e.pageX - _gx;//控件左上角到屏幕左上角的相对位置
		const y = e.pageY - _gy;
		$myBoxImg.css({"top":y,"left":x});
		console.log("移动");
	}
}).mouseup(function(e){
	endX = e.pageX;
	endY = e.pageY;
	let d = Math.sqrt((startX - endX) * (startX - endX) + (startY - endY) * (startY - endY));
	console.log("d:",d);
	if (d === 0 || d < 7) {
		console.log("执行了点击事件");
	} else {
		console.log("执行了拖拽事件");
	}
	isMove=false;
});