# Chrome-plug-in

#### 介绍
Chrome浏览器插件
javascript实现


#### 软件架构
软件架构说明


#### 安装教程

1.  git clone https://gitee.com/zheng_yongtao/chrome-plug-in.git
2.  查看对应项目的readme文件
3.  根据教程修改相关文件配置
4.  将文件夹拉进chrome扩展
5.  给我点个星星，刷新页面就可看到效果了

#### 使用说明

##### 1.Chrome-backGround 浏览器背景图

Chrome浏览器桌面换肤插件，可以选择自己喜欢的图片作为浏览器页面的背景，打造根据个性化的Chrome浏览器。
效果：
![效果图](https://img-blog.csdnimg.cn/6189930ddc604353a4a0f6f87e820497.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAU0FET05fanVuZw==,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)

![效果图](https://img-blog.csdnimg.cn/347270b3d6774c5e9568633dc74d3795.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAU0FET05fanVuZw==,size_12,color_FFFFFF,t_70,g_se,x_16#pic_center)

##### 2.Chrome-webPet 浏览器宠物

在Chrome中打造一只类似qq宠物的浏览器页面挂件，目前只展示，后续可以融入一些交互性功能。
效果：
![效果图](https://i.loli.net/2021/08/17/npO4dhj1kbH9WMg.png "在这里输入图片标题")

##### 3.Chrome-fireworkClick 浏览器烟花点击效果

浏览器页面点击烟花特效，让你的浏览器页面更加炫酷。
效果：
![效果图](https://i.loli.net/2021/08/17/zyP1bUQg4vDRpuT.png "在这里输入图片标题")

##### 4.chrome-backGroundVideo 浏览器背景视频

将B站视频设置成浏览器背景视频
![效果图](https://img-blog.csdnimg.cn/286b84897cc144b5a77006bd4d1874b0.gif#pic_center "在这里输入图片标题")

##### 5.chrome-tools-plugin 浏览器便捷助手
稍稍模仿了alfred的一些功能，目前实现了快捷键跳转，GitLab项目过滤，窗口分屏功能，更多功能正在设计开发中……
![输入图片说明](Chrome-tools-plugin/readmeImg/%E5%BF%AB%E6%8D%B7%E8%B7%B3%E8%BD%AC.gif)

##### 6.Chrome-Mid-Autumn Festival-click 鼠标中秋点击特效

##### 7.chrome插件模板

##### 8、chrome-bookmarks-manage chrome浏览器书签同步管理工具


有问题，有需求，有建议都可以联系我。
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

